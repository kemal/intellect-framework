<?php

require __DIR__ . '/../vendor/autoload.php';

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Matcher\UrlMatcher;
use Symfony\Component\Routing\RequestContext;
use Symfony\Component\Routing\RouteCollection;
use Symfony\Component\Routing\Route;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\Routing\Loader\YamlFileLoader;
use Intellect\Framework;

/*
$config = include __DIR__ . '/config/config.php';
$aliases = $config['aliases'];
Framework\IntellectLoader::getInstance($aliases)->register();

//\Intellect\View\View::Render("dasd");

View::Render("kemal");
*/

$locator = new FileLocator(array(__DIR__));
$loader = new YamlFileLoader($locator);
$routes = $loader->load('config/routing.yml');


$context = new RequestContext();
$request = Request::createFromGlobals();
// this is optional and can be done without a Request instance
$context->fromRequest(Request::createFromGlobals());
$matcher = new UrlMatcher($routes, $context);

try {
    $r = $matcher->match($request->getPathInfo());
    if (!$r)
        echo "404 Not Found";
    $modules_array = include __DIR__ . '/config/modules.php';
    require __DIR__ . '/../modules/' . $r['module'] . '/Module.php';
    $module = new $modules_array[$r['module']];
    $module->load($r['controller'], $r['method']);
} catch (Exception $e) {
    echo $e->getMessage();
}
