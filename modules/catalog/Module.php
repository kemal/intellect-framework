<?php
namespace Catalog;
use Catalog\Controller;

class Module {

    public function load($controller, $method) {

        $config = $this->getConfig();
        $controller_class = new $config[$controller];
        $controller_class->$method();
    }

    public function getConfig() {
        return include __DIR__ . '/config/module.config.php';
    }

}

