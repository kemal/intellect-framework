<?php
namespace Catalog\Controller;
use RedBean_Facade as R;

class BaseController extends \Intellect\Controller\Controller  {

        public $data = array();
	/**
	 * Message bag.
	 *
	 * @var Illuminate\Support\MessageBag
	 */
	protected $messageBag = null;

	/**
	 * Initializer.
	 *
	 * @return void
	 */
	public function __construct()
	{
		 $this->data['module'] = 'catalog';
	}

        public function getMessage(){
            
            return "super";
        }
	/**
	 * Setup the layout used by the controller.
	 *
	 * @return void
	 */
	protected function setupLayout()
	{
		if ( ! is_null($this->layout))
		{
			$this->layout = View::make($this->layout);
		}
	}

}