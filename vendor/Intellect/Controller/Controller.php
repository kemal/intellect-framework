<?php

namespace Intellect\Controller;
use Symfony\Component\HttpFoundation\Response;
use Intellect\View;

class Controller {

    static function view($page = 'index', $data = array()) {
        
        if (!empty($page) && is_array($page)) {
            $page = 'index';
        }
        $response = new Response(View\View::Twig($page, $data));
        $response->headers->set('Content-Type', 'text/html');
        $response->send();
    }

}

