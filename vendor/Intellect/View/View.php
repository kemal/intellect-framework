<?php

namespace Intellect\View;
class View {
    static function Twig($page = 'index', $data = array()) {
        $loader = new \Twig_Loader_Filesystem(__DIR__ . '/../../../modules/' . $data['module'] . '/views');
        $twig = new \Twig_Environment($loader, array(
                    'cache' => '../cache',
                    'debug' => true
                ));

        if (!empty($page) && is_array($page)) {
            $page = 'index';
        }
        return $twig->render($page . '.html', $data);
    }

}

